import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/data/core/api_constant.dart';

@injectable
class ApiClient{
  final Dio dio;
  ApiClient(this.dio);

  Future get(String path)async{
    String url = '${ApiConstant.baseUrl}$path?api_key=${ApiConstant.apiToken}';
    final response = await dio.get(url);
    return jsonDecode(jsonEncode(response.data));
  }
}