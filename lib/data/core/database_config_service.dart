import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

const String userTable = 'USER';

Future<Database> initializeLocalDatabase()async{
  Directory directory = await getApplicationDocumentsDirectory();
  String databasePath = join(directory.path,'local.db');
  Database database = await openDatabase(databasePath,version: 1,onCreate: onCreateDatabase);
  return database;
}
Future onCreateDatabase(Database db,int version)async{
  await db.execute('CREATE TABLE $userTable (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL)');
}