import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@lazySingleton
class PreferenceService{
  late final SharedPreferences preferences;
  PreferenceService(this.preferences);
   Future storeValueString(String key, String value)async{
    preferences.setString(key, value);
  }
   Future storeValueBool(String key, bool value)async{
    preferences.setBool(key, value);
  }
  Future<String?> getValueString(String key)async{
    return preferences.getString(key);
  }
   Future<bool?> getValueBool(String key)async{
    return preferences.getBool(key);
  }
  Future deleteValue(String key)async{
    await preferences.remove(key);
  }

}