import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:majootestcase/data/core/database_config_service.dart';
import 'package:majootestcase/data/core/preferences_service.dart';
import 'package:majootestcase/di/service_locator.dart';
import 'package:majootestcase/domain/entities/duplicate_data_exception.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:sqflite/sqflite.dart';

abstract class UserLocalDataSource{
  Future<bool> register(Map<String,dynamic> params);
  Future<UserEntity> login(String email,String password);
  Future<UserEntity> getCurrentUser();
  Future<bool?> checkCurrentSession();
  Future<bool> logout();
}
@LazySingleton(as: UserLocalDataSource)
class UserLocalDataSourceImpl implements UserLocalDataSource{
  final Database _database;
  final PreferenceService _preferenceService;
  UserLocalDataSourceImpl(this._database, this._preferenceService);
  @override
  Future<UserEntity> login(String email, String password) async {
    List<Map<String,dynamic>> result = await _database.query(userTable,where: 'email = ?',whereArgs: [email],limit: 1);
    if(result.isEmpty){
      throw Exception('Email tidak ditemukan');
    }
    UserEntity userEntity = UserEntity.fromJson(result.first);
    if(password!=userEntity.password){
      throw Exception('Password tidak sama');
    }
    await _preferenceService.storeValueBool(Preference.IS_LOGGED_IN,true);
    String data = userEntity.toString();
    await _preferenceService.storeValueString(Preference.USER_INFO, data);
    return userEntity;
  }

  @override
  Future<bool> register(Map<String, dynamic> params) async {
    List<Map<String,dynamic>> result = await _database.query(userTable,where: 'email = ?',whereArgs: [params['email']],limit: 1);
    if(result.isNotEmpty){
      throw DuplicateDataException('Email sudah dipakai');
    }
    await _database.insert(userTable, params);
    return true;
  }


  @override
  Future<UserEntity> getCurrentUser() async{
    String data = (await getIt<PreferenceService>().getValueString(Preference.USER_INFO))!;
    UserEntity user = UserEntity.fromJson(jsonDecode(data));
    return user;
  }

  @override
  Future<bool?> checkCurrentSession() async{
    return await _preferenceService.getValueBool(Preference.IS_LOGGED_IN);
  }

  @override
  Future<bool> logout() async {
    await _preferenceService.deleteValue(Preference.IS_LOGGED_IN);
    await _preferenceService.deleteValue(Preference.USER_INFO);
    return true;
  }
}