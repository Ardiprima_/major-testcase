import 'package:injectable/injectable.dart';
import 'package:majootestcase/data/core/api_client.dart';
import 'package:majootestcase/data/models/movie_model.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';

abstract class MovieRemoteDataSource{
  Future<MovieModel> getTrending();
}
@LazySingleton(as: MovieRemoteDataSource)
class MovieRemoteDataSourceImpl implements MovieRemoteDataSource{
  final ApiClient _client;

  MovieRemoteDataSourceImpl(this._client);
  
  @override
  Future<MovieModel> getTrending() async{
    final response = await  _client.get('/trending/all/day');
    MovieModel model = MovieModel.fromJson(response);
    return model;
  }
  
}