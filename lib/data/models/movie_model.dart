
import 'dart:convert';

import 'package:majootestcase/data/core/api_constant.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';

MovieModel movieModelFromJson(String str) => MovieModel.fromJson(json.decode(str));

String movieModelToJson(MovieModel data) => json.encode(data.toJson());

class MovieModel {
  MovieModel({
    required this.page,
    required this.results,
    required this.totalPages,
    required this.totalResults,
  });

  int page;
  List<Result> results;
  int totalPages;
  int totalResults;

  factory MovieModel.fromJson(Map<String, dynamic> json) => MovieModel(
    page: json["page"],
    results: List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    totalPages: json["total_pages"],
    totalResults: json["total_results"],
  );

  Map<String, dynamic> toJson() => {
    "page": page,
    "results": List<dynamic>.from(results.map((x) => x.toJson())),
    "total_pages": totalPages,
    "total_results": totalResults,
  };
}
class Result extends MovieEntity {
  Result({
    required this.originalLanguage,
    required this.posterPath,
    this.firstAirDate,
    required this.id,
    required this.overview,
    required this.voteCount,
    required this.voteAverage,
    required this.genreIds,
    this.name,
    required this.backdropPath,
    required this.originalName,
    required this.originCountry,
    required this.popularity,
    required this.mediaType,
    required this.originalTitle,
    this.releaseDate,
    required this.title,
    required this.adult,
    required this.video,
  }) : super(
      id: id,
      title: title,
      poster: '${ApiConstant.imageUrl}$posterPath',
    backdrop: '${ApiConstant.imageUrl}$backdropPath',
    overview: overview,
    popularity: popularity,
    rating: voteAverage,
    voteCount: voteCount,
    year: firstAirDate?.year??releaseDate?.year??DateTime.now().year
  );

  final String originalLanguage;
  final String posterPath;
  final DateTime? firstAirDate;
  final int id;
  final String overview;
  final int voteCount;
  final double voteAverage;
  final List<int> genreIds;
  final String? name;
  final String backdropPath;
  final String originalName;
  final List<String> originCountry;
  final double popularity;
  final String mediaType;
  final String originalTitle;
  final DateTime? releaseDate;
  final String title;
  final bool adult;
  final bool video;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    originalLanguage: json["original_language"]??'',
    posterPath: json["poster_path"]??'',
    firstAirDate: json['first_air_date']==null?null: DateTime.parse(json["first_air_date"]),
    id: json["id"],
    overview: json["overview"]??'',
    voteCount: json["vote_count"]??'',
    voteAverage: json["vote_average"]?.toDouble()??0.0,
    genreIds: List<int>.from(json["genre_ids"]?.map((x) => x)??[]),
    name: json["name"],
    backdropPath: json["backdrop_path"]??'',
    originalName: json["original_name"]??'',
    originCountry:  List<String>.from(json["origin_country"]?.map((x) => x)??[]),
    popularity: json["popularity"]?.toDouble()??0.0,
    mediaType: json["media_type"]??'',
    originalTitle: json["original_title"]??'',
    releaseDate: json['release_data']==null?null: DateTime.parse(json["release_date"]),
    title:json["title"]??json["name"]??'',
    adult: json["adult"]??false,
    video:  json["video"]??false,
  );

  Map<String, dynamic> toJson() => {
    "original_language": originalLanguage,
    "poster_path": posterPath,
    "id": id,
    "overview": overview,
    "vote_count": voteCount,
    "vote_average": voteAverage,
    "genre_ids": List<dynamic>.from(genreIds.map((x) => x)),
    "name": name,
    "backdrop_path": backdropPath,
    "original_name": originalName,
    "origin_country":  List<dynamic>.from(originCountry.map((x) => x)),
    "popularity": popularity,
    "media_type": mediaType,
    "original_title": originalTitle,
    "title": title,
    "adult":  adult,
    "video":  video,
  };
}
