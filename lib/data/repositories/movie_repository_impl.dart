import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/data/data_sources/remote_data_source/movie_remote_data_source.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';
import 'package:majootestcase/domain/repositories/movie_repository.dart';
import 'package:majootestcase/utils/error_helper.dart';

@LazySingleton(as: MovieRepository)
class MovieRepositoryImpl implements MovieRepository{
  final MovieRemoteDataSource _remoteDataSource;

  MovieRepositoryImpl(this._remoteDataSource);

  @override
  Future<Either<AppError, List<MovieEntity>>> getTrending() async {
    try{
      final list = await _remoteDataSource.getTrending();
      return Right(list.results);
    }on SocketException catch(_){
      return const Left(AppError(AppErrorType.network,message: 'Cannot connect to server. Make sure you have proper internet connection'));
    }on DioError catch(e){
      return  Left(AppError(AppErrorType.api,message: ErrorHelper.extractApiError(e)));
    }on Exception{
      return  Left(AppError(AppErrorType.api,message: 'Something went wrong'));
    }
  }
}