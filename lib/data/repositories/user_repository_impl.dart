import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/data/core/preferences_service.dart';
import 'package:majootestcase/data/data_sources/local_data_source/user_local_data_source.dart';
import 'package:majootestcase/di/service_locator.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/duplicate_data_exception.dart';
import 'package:majootestcase/domain/entities/login_params.dart';
import 'package:majootestcase/domain/entities/register_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';

import '../../utils/constant.dart';

@LazySingleton(as: UserRepository)
class UserRepositoryImpl implements UserRepository{
  final UserLocalDataSource _localDataSource;
  UserRepositoryImpl(this._localDataSource);

  @override
  Future<Either<AppError, UserEntity>> login(LoginParams params) async {
    try{
      UserEntity userEntity = await _localDataSource.login(params.email,params.password);
      return Right(userEntity);
    } catch(e){
      print(e.toString());
      return Left(AppError(AppErrorType.database,message: e.toString()));
    }
  }

  @override
  Future<Either<AppError, bool>> register(RegisterParams params) async{
    try{
      bool success = await _localDataSource.register(params.toJson());
      return Right(success);
    }on DuplicateDataException catch(e){
      return Left(AppError(AppErrorType.database,message: e.message));
    }
    catch(e){
      return Left(AppError(AppErrorType.database,message: 'An Error Occured when connecting to database'));
    }
  }

  @override
  Future<Either<AppError, UserEntity>> getCurrentUser()async {
    try{
      UserEntity user = await _localDataSource.getCurrentUser();
      return Right(user);
    }catch(_){
      return Left(AppError(AppErrorType.database,message: 'An Error Occured'));
    }
  }

  @override
  Future<Either<AppError, bool?>> checkCurrentSession() async {
    try{
      bool? check = await _localDataSource.checkCurrentSession();
      return Right(check);
    }catch(_){
      return Left(AppError(AppErrorType.database,message: 'An Error Occured'));
    }
  }

  @override
  Future<Either<AppError, bool>> logout() async{
    try{
      bool logout = await _localDataSource.logout();
      return Right(logout);
    }catch(_){
      return Left(AppError(AppErrorType.database,message: 'An Error Occured'));
    }
  }
}