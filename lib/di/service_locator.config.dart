// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i9;
import 'package:sqflite/sqflite.dart' as _i11;

import '../data/core/api_client.dart' as _i3;
import '../data/core/preferences_service.dart' as _i8;
import '../data/data_sources/local_data_source/user_local_data_source.dart'
    as _i10;
import '../data/data_sources/remote_data_source/movie_remote_data_source.dart'
    as _i5;
import '../data/repositories/movie_repository_impl.dart' as _i7;
import '../data/repositories/user_repository_impl.dart' as _i13;
import '../domain/repositories/movie_repository.dart' as _i6;
import '../domain/repositories/user_repository.dart' as _i12;
import '../domain/usecases/check_current_session.dart' as _i14;
import '../domain/usecases/get_current_user.dart' as _i15;
import '../domain/usecases/get_trending.dart' as _i16;
import '../domain/usecases/login.dart' as _i18;
import '../domain/usecases/logout.dart' as _i19;
import '../domain/usecases/register.dart' as _i20;
import '../presentation/bloc/auth/auth_cubit.dart' as _i21;
import '../presentation/bloc/home/home_cubit.dart' as _i17;
import '../presentation/bloc/register/register_cubit.dart'
    as _i22; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.ApiClient>(() => _i3.ApiClient(get<_i4.Dio>()));
  gh.lazySingleton<_i5.MovieRemoteDataSource>(
      () => _i5.MovieRemoteDataSourceImpl(get<_i3.ApiClient>()));
  gh.lazySingleton<_i6.MovieRepository>(
      () => _i7.MovieRepositoryImpl(get<_i5.MovieRemoteDataSource>()));
  gh.lazySingleton<_i8.PreferenceService>(
      () => _i8.PreferenceService(get<_i9.SharedPreferences>()));
  gh.lazySingleton<_i10.UserLocalDataSource>(() => _i10.UserLocalDataSourceImpl(
      get<_i11.Database>(), get<_i8.PreferenceService>()));
  gh.lazySingleton<_i12.UserRepository>(
      () => _i13.UserRepositoryImpl(get<_i10.UserLocalDataSource>()));
  gh.lazySingleton<_i14.CheckCurrentSession>(
      () => _i14.CheckCurrentSession(get<_i12.UserRepository>()));
  gh.lazySingleton<_i15.GetCurrentUser>(
      () => _i15.GetCurrentUser(get<_i12.UserRepository>()));
  gh.factory<_i16.GetTrending>(
      () => _i16.GetTrending(get<_i6.MovieRepository>()));
  gh.factory<_i17.HomeCubit>(() =>
      _i17.HomeCubit(get<_i16.GetTrending>(), get<_i15.GetCurrentUser>()));
  gh.lazySingleton<_i18.Login>(() => _i18.Login(get<_i12.UserRepository>()));
  gh.lazySingleton<_i19.Logout>(() => _i19.Logout(get<_i12.UserRepository>()));
  gh.factory<_i20.Register>(() => _i20.Register(get<_i12.UserRepository>()));
  gh.factory<_i21.AuthCubit>(() => _i21.AuthCubit(
      get<_i18.Login>(), get<_i19.Logout>(), get<_i14.CheckCurrentSession>()));
  gh.factory<_i22.RegisterCubit>(
      () => _i22.RegisterCubit(get<_i20.Register>(), get<_i21.AuthCubit>()));
  return get;
}
