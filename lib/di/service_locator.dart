import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:dio/dio.dart';
import 'package:majootestcase/data/core/database_config_service.dart';
import 'package:majootestcase/data/core/dio_config_service.dart';
import 'package:majootestcase/di/service_locator.config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
final getIt = GetIt.I;
@InjectableInit(
    initializerName: r'$initGetIt',
    preferRelativeImports: true,
    asExtension: false
)
Future configureDependencies()async{
  getIt.registerLazySingleton<Dio>(()=>createInstance());
  await initializeLocalDatabase().then((value) => getIt.registerLazySingleton<Database>(() => value));
  await SharedPreferences.getInstance().then((value) => getIt.registerLazySingleton<SharedPreferences>(() => value));
  $initGetIt(getIt);
}