class DuplicateDataException implements Exception{
  final String message;

  DuplicateDataException(this.message);
}