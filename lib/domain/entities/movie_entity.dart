import 'package:equatable/equatable.dart';

class MovieEntity extends Equatable{
  final int id;
  final String poster;
  final String backdrop;
  final String title;
  final int year;
  final double rating;
  final int voteCount;
  final String overview;
  final double popularity;

  MovieEntity(
      {required this.id,
      required this.poster,
        required this.backdrop,
      required this.title,
      required this.year,
      required this.rating,
      required this.overview,
        required this.voteCount,
      required this.popularity});

  @override
  List<Object?> get props => [
    id,
    title,
    poster,
    backdrop,
    year,
    rating,
    overview,
    popularity
  ];
}