import 'dart:convert';

class UserEntity {
  String? email;
  String? userName;
  String? password;

  UserEntity({this.email, this.userName, this.password});

  UserEntity.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() =>
      {
        'email': email,
        'password': password,
        'username': userName
      };

  @override
  toString() => jsonEncode(toJson());
}