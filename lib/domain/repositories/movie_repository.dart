import 'package:dartz/dartz.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';

abstract class MovieRepository{
  Future<Either<AppError,List<MovieEntity>>> getTrending();
}