import 'package:dartz/dartz.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/login_params.dart';
import 'package:majootestcase/domain/entities/register_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';

abstract class UserRepository{
  Future<Either<AppError,UserEntity>> login(LoginParams params);
  Future<Either<AppError,bool>> register(RegisterParams params);
  Future<Either<AppError,UserEntity>> getCurrentUser();
  Future<Either<AppError,bool?>> checkCurrentSession();
  Future<Either<AppError,bool>> logout();
}