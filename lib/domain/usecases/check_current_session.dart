import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/no_params.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';
import 'package:majootestcase/domain/usecases/usecase.dart';

@lazySingleton
class CheckCurrentSession extends UseCase<bool?,NoParams>{
  final UserRepository _repository;

  CheckCurrentSession(this._repository);
  @override
  Future<Either<AppError, bool?>> call(NoParams params) async {
    return await _repository.checkCurrentSession();
  }
}