import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/no_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';
import 'package:majootestcase/domain/usecases/usecase.dart';

@lazySingleton
class GetCurrentUser extends UseCase<UserEntity,NoParams>{
  final UserRepository _repository;

  GetCurrentUser(this._repository);
  @override
  Future<Either<AppError, UserEntity>> call(NoParams params)async {
    return await _repository.getCurrentUser();
  }

}