import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';
import 'package:majootestcase/domain/entities/no_params.dart';
import 'package:majootestcase/domain/repositories/movie_repository.dart';
import 'package:majootestcase/domain/usecases/usecase.dart';

@injectable
class GetTrending extends UseCase<List<MovieEntity>,NoParams>{
  final MovieRepository _repository;

  GetTrending(this._repository);
  @override
  Future<Either<AppError, List<MovieEntity>>> call(NoParams params)async {
    return await _repository.getTrending();
  }
}