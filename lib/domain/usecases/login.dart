import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/login_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';
import 'package:majootestcase/domain/usecases/usecase.dart';

@lazySingleton
class Login extends UseCase<UserEntity,LoginParams>{
  final UserRepository _repository;
  Login(this._repository);
  @override
  Future<Either<AppError, UserEntity>> call(LoginParams params) async {
    return await _repository.login(params);
  }
}