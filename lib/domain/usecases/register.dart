import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/register_params.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';
import 'package:majootestcase/domain/usecases/usecase.dart';

@injectable
class Register extends UseCase<bool,RegisterParams>{
  final UserRepository _repository;
  Register(this._repository);
  @override
  Future<Either<AppError, bool>> call(RegisterParams params)async {
    return await _repository.register(params);
  }
}