import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/di/service_locator.dart';
import 'package:majootestcase/presentation/ui/home_bloc/home_screen.dart';
import 'package:majootestcase/presentation/ui/login/login_page.dart';
import 'package:majootestcase/presentation/ui/routes.dart';
import 'package:majootestcase/utils/constant.dart';

Future main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(ScreenUtilConstants.width,ScreenUtilConstants.height,),
      builder:()=> MaterialApp(
        title: 'Flutter Demo',
        onGenerateRoute: AppRouter().onGenerateRoute,
        theme: ThemeData(
          primaryColor: kPrimaryColor,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        builder: EasyLoading.init(),
      ),
    );
  }
}

