
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/entities/login_params.dart';
import 'package:majootestcase/domain/entities/no_params.dart';
import 'package:majootestcase/domain/usecases/check_current_session.dart';
import 'package:majootestcase/domain/usecases/login.dart';
import 'package:majootestcase/domain/usecases/logout.dart';

part 'auth_state.dart';

@injectable
class AuthCubit extends Cubit<AuthState> {
  final Login login;
  final Logout logoutCase;
  final CheckCurrentSession checkCurrentSession;
  AuthCubit(this.login, this.logoutCase, this.checkCurrentSession) : super(AuthInitialState());

  Future fetchHistoryLogin() async{
    emit(AuthInitialState());
    final eith = await checkCurrentSession.call(NoParams());
    eith.fold((l){
      emit(AuthLoginState());
    }, (r){
      if(r== null){
        emit(AuthLoginState());
      }else{
        if(r){
          emit(AuthLoggedInState());
        }else{
          emit(AuthLoginState());
        }
      }
    });
  }

  Future loginUser(LoginParams user) async{
    emit(AuthLoadingState());
    final eith = await login.call(LoginParams(email: user.email, password: user.password));
    eith.fold((l) => emit(AuthErrorState('Login gagal, periksa kembali inputan anda')), (r) => emit(AuthSuccesState()));
  }
  Future logout()async{
    emit(AuthLoadingState());
    await logoutCase.call(NoParams());
    emit(AuthLogoutState());
  }
}
