part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
    List<Object> get props => [];
}

class AuthInitialState extends AuthState { }

class AuthLoadingState extends AuthState { }

class AuthLoggedInState extends AuthState { }

class AuthLoginState extends AuthState { }
class AuthLogoutState extends AuthState { }

class AuthSuccesState extends AuthState { }


class AuthErrorState extends AuthState {
    final String error;

    AuthErrorState(this.error);

    @override
    List<Object> get props => [error];
}
