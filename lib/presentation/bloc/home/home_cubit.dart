
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';
import 'package:majootestcase/domain/entities/no_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/domain/usecases/get_current_user.dart';
import 'package:majootestcase/domain/usecases/get_trending.dart';
part 'home_state.dart';

@injectable
class HomeCubit extends Cubit<HomeState> {
  final GetTrending getTrending;
  final GetCurrentUser getCurrentUser;
  HomeCubit(this.getTrending, this.getCurrentUser) : super(HomeInitialState());
  Future fetchData() async {
   emit(HomeLoadingState());
   final eithUser = await getCurrentUser.call(NoParams());
   UserEntity user = eithUser.toOption().toNullable()!;
   final eith = await getTrending.call(NoParams());
   emit(eith.fold((l) => HomeErrorState(l.message), (r) => HomeLoadedState(r,user)));
  }
}
