part of 'home_cubit.dart';


abstract class HomeState extends Equatable {
  const HomeState();

  @override
    List<Object?> get props => [];
}

class HomeInitialState extends HomeState { }

class HomeLoadingState extends HomeState { }

class HomeLoadedState extends HomeState {
    final List<MovieEntity> data;
    final UserEntity user;
    HomeLoadedState(this.data,this.user);
    @override
    List<Object?> get props => [data,user];
}

class HomeErrorState extends HomeState {
    final String error;

    HomeErrorState(this.error);

    @override
    List<Object> get props => [error];
}
