import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/entities/login_params.dart';
import 'package:majootestcase/domain/entities/register_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/domain/usecases/register.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_cubit.dart';

part 'register_state.dart';

@injectable
class RegisterCubit extends Cubit<RegisterState> {
  final AuthCubit authCubit;
  final Register register;
  RegisterCubit(this.register, this.authCubit) : super(RegisterInitial());
  Future registerUser(RegisterParams params)async{
    emit(RegisterLoading());
    final eith = await register.call(params);
    eith.fold((l) => emit(RegisterFailure(l.message)), (_) async {
      await authCubit.loginUser(LoginParams(email: params.email, password: params.password));
      emit(RegisterSuccess());
    });
  }
}
