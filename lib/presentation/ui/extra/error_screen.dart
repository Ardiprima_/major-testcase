import 'package:flutter/material.dart';
import 'package:majootestcase/utils/constant.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.retryButton,
      this.message="",
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              message,
              style: blackTextStyle.copyWith(
                  fontSize: 12, color: textColor ?? Colors.black),
            ),
            retry != null
                ? Column(
                    children: [
                      SizedBox(
                        height: 50,
                      ),
                      retryButton ??
                          IconButton(
                            onPressed: () {
                              if(retry!=null)
                                retry!();
                            },
                            icon: Icon(Icons.refresh_sharp),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
