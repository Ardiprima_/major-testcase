import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/data/models/movie_model.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_cubit.dart';
import 'package:majootestcase/utils/constant.dart';

class HomeLoadedScreen extends StatelessWidget {
   final List<MovieEntity> data;
   final UserEntity user;
  const HomeLoadedScreen({Key? key, required this.data, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Selamat Datang, ${user.userName}',style: blackTextStyle.copyWith(
                      fontWeight: extraBold,
                      fontSize: 24
                    ),),
                    IconButton(onPressed: (){
                      context.read<AuthCubit>().logout();
                    }, icon: Icon(Icons.exit_to_app))
                  ],
                ),
                SizedBox(height: 20,),
                Text('Popular Movie',style: blackTextStyle.copyWith(
                    fontWeight: bold,
                    fontSize: 18
                ),),
                SizedBox(height: 20,),
                GridView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    childAspectRatio: 0.6
                  ),
                  shrinkWrap: true,
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return movieItemWidget(data[index],context);
                },
    ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget movieItemWidget(MovieEntity data, BuildContext context){
    return InkWell(
      onTap: (){
        Navigator.pushNamed(context, RouteList.movieDetail,arguments: data);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(child: Image.network(data.poster),borderRadius: BorderRadius.circular(20),),
          SizedBox(height: 5,),
          Expanded(child: Text(data.title, textDirection: TextDirection.ltr,maxLines: 1,overflow: TextOverflow.ellipsis,style: blackTextStyle.copyWith(fontSize: 14),))
        ],
      ),
    );
  }
}
