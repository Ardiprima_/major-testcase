import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/di/service_locator.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_cubit.dart';
import 'package:majootestcase/presentation/bloc/home/home_cubit.dart';
import 'package:majootestcase/presentation/ui/home_bloc/home_loaded_screen.dart';
import 'package:majootestcase/utils/constant.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeCubit _homeCubit;
  late AuthCubit _authCubit;

  @override
  void initState() {
    super.initState();
    _homeCubit = getIt<HomeCubit>()..fetchData();
    _authCubit = getIt<AuthCubit>();
  }

  @override
  void dispose() {
    super.dispose();
    _homeCubit.close();
    _authCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_)=>_homeCubit,
        ),
        BlocProvider(
          create: (_)=>_authCubit,
        ),
      ],
      child: BlocListener<AuthCubit,AuthState>(
        listener: (context,state){
          if(state is AuthLogoutState){
            Navigator.pushNamedAndRemoveUntil(context, RouteList.home,(route)=>false);
          }
        },
        child: BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state)
        {
          if(state is HomeLoadedState)
            {
              return HomeLoadedScreen(data: state.data,user: state.user,);
            }
          else if(state is HomeLoadingState)
            {
              return LoadingIndicator();
            }

          else if(state is HomeInitialState)
            {
              return Scaffold();
            }

          else if(state is HomeErrorState)
            {
              return ErrorScreen(message: state.error,retry: _homeCubit.fetchData,);
            }

          return Center(child: Text(
            kDebugMode?"state not implemented $state": ""
          ));
        }),
      ),
    );
  }
}
