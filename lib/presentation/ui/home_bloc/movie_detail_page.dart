import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';
import 'package:majootestcase/utils/constant.dart';

class MovieDetailPage extends StatelessWidget {
  const MovieDetailPage({Key? key, required this.movie}) : super(key: key);
  final MovieEntity movie;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(color: kBlackColor),
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 300.h,
                  width: MediaQuery.of(context).size.width,
                  child: ClipRRect(child: Image.network(movie.poster),borderRadius: BorderRadius.circular(20),),
                ),
                SizedBox(height: 20,),
                Text('${movie.title} (${movie.year})',style: blackTextStyle.copyWith(fontSize: 18,fontWeight: bold),),
                Row(
                  children: [
                    Icon(Icons.star,color: Colors.yellow[700],size: 20,),
                    Text('${movie.rating} (${movie.voteCount})',style: greyTextStyle.copyWith(fontSize: 12),)
                  ],
                ),
                Text('Popularity : ${movie.popularity}',style: blackTextStyle),
                const Divider(),
                Text('Overview',style: blackTextStyle.copyWith(fontSize: 14),),
                Text(movie.overview,style: greyTextStyle.copyWith(fontSize: 12),textAlign: TextAlign.justify,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
