import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:majootestcase/di/service_locator.dart';
import 'package:majootestcase/domain/entities/login_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_cubit.dart';
import 'package:majootestcase/presentation/common/widget/custom_button.dart';
import 'package:majootestcase/presentation/common/widget/text_form_field.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/form_validation.dart';



class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  bool _isObscurePassword = true;
  late AuthCubit _authCubit;

  @override
  void initState() {
    super.initState();
    _authCubit = getIt<AuthCubit>();

  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_)=>_authCubit,
      child: BlocListener<AuthCubit,AuthState>(
        listener: (context,state){
          if(state is AuthLoadingState){
            showLoadingCircle();
          }else if(state is AuthSuccesState){
            EasyLoading.dismiss();
            EasyLoading.showSuccess('Login berhasil');
            Navigator.pushNamedAndRemoveUntil(context, RouteList.dashboard,(route)=>false);
          }else if(state is AuthErrorState){
            EasyLoading.dismiss();
            EasyLoading.showError(state.error);
          }
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Selamat Datang',
                    style: blackTextStyle.copyWith(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      // color: colorBlue,
                    ),
                  ),
                  Text(
                    'Silahkan login terlebih dahulu',
                    style: blackTextStyle.copyWith(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  _form(),
                  SizedBox(
                    height: 50,
                  ),
                  CustomButton(
                    text: 'Login',
                    onPressed: (){
                      handleLogin();
                    },
                    height: 100,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  _register(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: FormValidation.validateEmail,
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            validator:(val)=> FormValidation.validate(val, label: 'Password'),
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.pushNamed(context, RouteList.register);
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    if((_email??'').isEmpty&&(_password??'').isEmpty){
      EasyLoading.showError('Login gagal , periksa kembali inputan anda');
    }
    if(formKey.currentState?.validate()??false){
        _authCubit.loginUser(
            LoginParams(email: _email ?? '', password: _password ?? ''));
    }
  }
}
