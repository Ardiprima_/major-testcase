import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/di/service_locator.dart';
import 'package:majootestcase/presentation/ui/extra/loading.dart';

import '../bloc/auth/auth_cubit.dart';
import 'home_bloc/home_screen.dart';
import 'login/login_page.dart';

class MyHomePageScreen extends StatefulWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  State<MyHomePageScreen> createState() => _MyHomePageScreenState();
}

class _MyHomePageScreenState extends State<MyHomePageScreen> {
  late AuthCubit _authCubit;
  @override
  void initState() {
    super.initState();
    _authCubit = getIt<AuthCubit>()..fetchHistoryLogin();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_)=>_authCubit,
      child: BlocBuilder<AuthCubit, AuthState>(
          builder: (context, state)
          {
            if(state is AuthLoginState)
            {
              return LoginPage();
            }
            else if(state is AuthLoggedInState)
            {
              return HomeScreen();
            }
            return LoadingIndicator();
          }),
    );
  }
}
