import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:majootestcase/di/service_locator.dart';
import 'package:majootestcase/domain/entities/register_params.dart';
import 'package:majootestcase/presentation/bloc/register/register_cubit.dart';
import 'package:majootestcase/presentation/common/widget/custom_button.dart';
import 'package:majootestcase/presentation/common/widget/text_form_field.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/form_validation.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  late RegisterCubit _registerCubit;
  final _form = GlobalKey<FormState>();
  TextController _emailController = TextController();
  TextController _passwordController = TextController();
  TextController _usernameController = TextController();
  late bool _obscurePassword;

  @override
  void initState() {
    super.initState();
    _obscurePassword = true;
    _registerCubit = getIt<RegisterCubit>();
  }

  @override
  void dispose() {
    super.dispose();
    _registerCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_)=>_registerCubit,
      child: BlocListener<RegisterCubit,RegisterState>(
        listener: (context,state){
          if(state is RegisterLoading){
            showLoadingCircle();
          }else if(state is RegisterFailure){
            EasyLoading.dismiss();
            EasyLoading.showError(state.message);
          }else{
            EasyLoading.dismiss();
            EasyLoading.showSuccess('Register berhasil');
            Navigator.pushNamedAndRemoveUntil(context, RouteList.dashboard,(route)=>false);
          }
        },
        child: Scaffold(
          body: SafeArea(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 75,left: 20,right: 20),
                child: Form(
                  key: _form,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Selamat Datang',
                        style: blackTextStyle.copyWith(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          // color: colorBlue,
                        ),
                      ),
                      Text(
                        'Silahkan daftar untuk melanjutkan',
                        style: blackTextStyle.copyWith(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      const SizedBox(height: 20,),
                      CustomTextFormField(
                        context: context,
                        controller: _emailController,
                        isEmail: true,
                        hint: 'Masukkan email',
                        label: 'Email',
                        validator:FormValidation.validateEmail,
                      ),
                      CustomTextFormField(
                        context: context,
                        controller: _usernameController,
                        label: 'Username',
                        validator: (val)=>FormValidation.validate(val, label: 'Username'),
                        hint: 'Masukkan username',
                      ),
                      CustomTextFormField(
                        context: context,
                        controller: _passwordController,
                        label: 'Password',
                        hint: 'Masukkan password',
                        validator: (val)=>FormValidation.validate(val, label: 'Password'),
                        isObscureText: _obscurePassword,
                        suffixIcon: IconButton(
                          icon: Icon(
                            _obscurePassword
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                          ),
                          onPressed: () {
                            setState(() {
                              _obscurePassword = !_obscurePassword;
                            });
                          },
                        ),
                      ),
                      SizedBox(height: 50,),
                      CustomButton(
                        text: 'Register',
                        onPressed: (){
                          String _email = _emailController.value??'';
                          String _password = _passwordController.value??'';
                          String _username = _usernameController.value??'';
                          if(_email.isEmpty&&_password.isEmpty&&_username.isEmpty){
                            EasyLoading.showError('Form tidak boleh kosong, mohon cek kembali data yang anda inputkan');
                          }
                          if(_form.currentState?.validate()??false){
                            _registerCubit.registerUser(RegisterParams(username:_username, email:_email, password: _password));
                          }
                        },
                        height: 100,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
