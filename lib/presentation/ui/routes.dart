import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';
import 'package:majootestcase/presentation/ui/home_bloc/home_screen.dart';
import 'package:majootestcase/presentation/ui/home_bloc/movie_detail_page.dart';
import 'package:majootestcase/presentation/ui/my_home_page.dart';
import 'package:majootestcase/presentation/ui/register/register_page.dart';
import 'package:majootestcase/utils/constant.dart';

class AppRouter{
  Route? onGenerateRoute(RouteSettings settings){
    switch(settings.name){
      case RouteList.home:
        return MaterialPageRoute(
            builder: (_) => MyHomePageScreen()
        );
      case RouteList.dashboard:
        return MaterialPageRoute(
            builder: (_) => HomeScreen()
        );
      case RouteList.register:
        return MaterialPageRoute(
          builder: (_) => RegisterPage()
        );
      case RouteList.movieDetail:
        return MaterialPageRoute(
            builder: (_) => MovieDetailPage(movie: settings.arguments! as MovieEntity)
        );
      default : return null;
    }
  }
}