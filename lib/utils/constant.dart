import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';

class Preference {
  static const USER_INFO = "user-info";
  static const IS_LOGGED_IN = "is-logged-in";
}


class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}
class RouteList{
  RouteList._();
  static const String register = '/register';
  static const String home = '/';
  static const String dashboard = '/dashboard';
  static const String movieDetail = '/movie-detail';
}


class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
Color kPrimaryColor = const Color(0xFF19B0A1);
Color kBlackColor = const Color(0xFF131313);
Color kWhiteColor = const Color(0xffFFFFFF);
Color kGreyColor = const Color(0xff9698A9);
FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;
TextStyle blackTextStyle = GoogleFonts.poppins(
    fontSize: 12,
    color: kBlackColor
);
TextStyle whiteTextStyle = GoogleFonts.poppins(
    fontSize: 12,
    color: kWhiteColor
);
TextStyle greyTextStyle = GoogleFonts.poppins(
    fontSize: 12,
    color: kGreyColor
);
void showLoadingCircle(){
  EasyLoading.show(status: 'Loading',dismissOnTap: false,maskType: EasyLoadingMaskType.black);
}