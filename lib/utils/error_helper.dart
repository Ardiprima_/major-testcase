
import 'package:dio/dio.dart';

class ErrorHelper {
  ErrorHelper._();
  static String getErrorMessage(error) {
    print('error helper === $error');
    String message=  "Something went wrong.";
    if (error is DioError) {
      message = error.message;
    }
    return message;
  }

  static String extractApiError(DioError error) {
    String message = "Something went wrong";
    print(
        'error === ${error.response}  ==== ${error.response != null ? error.response?.data : 'noresponse'} ==== ${error.response != null ? error.response?.extra : 'no response'}=== ${error.message}');
      if (error.response != null &&
          error.response?.data['message'] != null) {
        message = error.response?.data['message'];
      }else if(error.type == DioErrorType.connectTimeout||error.type == DioErrorType.receiveTimeout||error.type==DioErrorType.other){
        message = 'Cannot connect to server. Make sure you have proper internet connection';
      } else {
        message = error.message;
      }
    return message;
  }
}
