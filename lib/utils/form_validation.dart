class FormValidation{
  FormValidation._();
  static String? validateEmail(String? val){
      final pattern =  RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
      if (val != null) {
      return pattern.hasMatch(val) ? null : 'email is invalid';
    }
      return 'Email required';
  }
  static String? validate(String? val,{required String label}){
    if(val!=null&&val.isNotEmpty){
      return null;
    }else{
      return '$label required';
    }
  }
}