import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/login_params.dart';
import 'package:majootestcase/domain/entities/no_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/domain/usecases/check_current_session.dart';
import 'package:majootestcase/domain/usecases/login.dart';
import 'package:majootestcase/domain/usecases/logout.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_cubit.dart';

import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'auth_cubit_test.mocks.dart';

@GenerateMocks([Logout,CheckCurrentSession,Login])

main(){
  late MockLogin login;
  late MockLogout logout;
  late MockCheckCurrentSession checkCurrentSession;
  late AuthCubit authCubit;
  setUp((){
    WidgetsFlutterBinding.ensureInitialized();
    login = MockLogin();
    logout = MockLogout();
    checkCurrentSession = MockCheckCurrentSession();
    authCubit = AuthCubit(login, logout, checkCurrentSession);
  });
  tearDown((){
    authCubit.close();
  });
  blocTest('Should return AuthLoginState if user not logged in', build: ()=>authCubit,
    act: (AuthCubit authCubit)async{
      when(checkCurrentSession.call(NoParams())).thenAnswer((_)async => Right(null));
      await authCubit.fetchHistoryLogin();
    },
    expect: ()=>[
      isA<AuthInitialState>(),
      isA<AuthLoginState>(),
  ]
  );
  blocTest('Should return AuthLoggedInState if user logged in', build: ()=>authCubit,
      act: (AuthCubit authCubit)async{
        when(checkCurrentSession.call(NoParams())).thenAnswer((_)async => Right(true));
        await authCubit.fetchHistoryLogin();
      },
      expect: ()=>[
        isA<AuthInitialState>(),
        isA<AuthLoggedInState>(),
      ]
  );
  blocTest('Should return AuthSuccessState if user enter correct email and password', build: ()=>authCubit,
      act: (AuthCubit authCubit)async{
        when(login.call(any)).thenAnswer((_)async => Right(UserEntity(email: 'ardiprima27@gmail.com',userName: 'ardi',password: '12345678')));
        await authCubit.loginUser(LoginParams(email: 'ardiprima27@gmail.com', password: '12345678'));
      },
      expect: ()=>[
        isA<AuthLoadingState>(),
        isA<AuthSuccesState>(),
      ]
  );
  blocTest('Should return AuthFailureState if user enter wrong email or password', build: ()=>authCubit,
      act: (AuthCubit authCubit)async{
        when(login.call(any)).thenAnswer((_)async => Left(AppError(AppErrorType.database,message: 'wrong account')));
        await authCubit.loginUser(LoginParams(email: 'ardiprima27@gmail.com', password: '12345678'));
      },
      expect: ()=>[
        isA<AuthLoadingState>(),
        isA<AuthErrorState>(),
      ]
  );
  blocTest('Should return AuthLogoutState if user press logout button', build: ()=>authCubit,
      act: (AuthCubit authCubit)async{
        when(logout.call(any)).thenAnswer((_)async => Right(true));
        await authCubit.logout();
      },
      expect: ()=>[
        isA<AuthLoadingState>(),
        isA<AuthLogoutState>(),
      ]
  );
}