import 'dart:convert';

import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/movie_entity.dart';
import 'package:majootestcase/domain/entities/no_params.dart';
import 'package:majootestcase/domain/entities/user_entity.dart';
import 'package:majootestcase/domain/usecases/get_current_user.dart';
import 'package:majootestcase/domain/usecases/get_trending.dart';
import 'package:majootestcase/presentation/bloc/home/home_cubit.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'home_cubit_test.mocks.dart';

@GenerateMocks([GetTrending,GetCurrentUser])
main() {
  late MockGetTrending getTrendingMock;
  late MockGetCurrentUser getCurrentUser;
  late HomeCubit homeCubit;

  setUp(() {
    getTrendingMock = MockGetTrending();
    getCurrentUser = MockGetCurrentUser();

    homeCubit = HomeCubit(getTrendingMock,getCurrentUser);
  });
  tearDown(() {
    homeCubit.close();
  });
  blocTest('Should emit HomeLoaded State when api call success',
      build: () => homeCubit,
      act: (HomeCubit homeCubit) async {
        when(getTrendingMock.call(NoParams())).thenAnswer((_) async => Right([
          MovieEntity(
              id: 1,
              poster: 'poster',
              backdrop: 'backdrop',
              title: 'spiderman',
              year: 2022,
              rating: 8.9,
              overview: 'overview',
              voteCount: 200,
              popularity: 200.88)
        ]));
        when(getCurrentUser.call(NoParams())).thenAnswer((_)async => Right(UserEntity(userName: 'ardi',password: '12345678',email: 'ardiprima27@gmail.com')));
        await homeCubit.fetchData();
      },
      expect: () => [
            isA<HomeLoadingState>(),
            isA<HomeLoadedState>(),
          ],
      );
  blocTest('Should emit HomeErrorState when failure fetching data form api', build: ()=>homeCubit,
    act: (HomeCubit homeCubit)async{
      when(getCurrentUser.call(NoParams())).thenAnswer((_)async => Right(UserEntity(userName: 'ardi',password: '12345678',email: 'ardiprima27@gmail.com')));
      when(getTrendingMock.call(NoParams())).thenAnswer((_) async => Left(AppError(AppErrorType.api,message: 'An error occured')));
        await homeCubit.fetchData();
    },
    expect: ()=>[
      isA<HomeLoadingState>(),
      isA<HomeErrorState>()
    ]
  );
}
