
import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/domain/entities/app_error.dart';
import 'package:majootestcase/domain/entities/register_params.dart';
import 'package:majootestcase/domain/usecases/register.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_cubit.dart';
import 'package:majootestcase/presentation/bloc/register/register_cubit.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'auth_cubit_test.mocks.dart';
import 'register_cubit_test.mocks.dart';

@GenerateMocks([Register,AuthCubit])

main(){
  late RegisterCubit registerCubit;
  late MockRegister register;
  late MockAuthCubit authCubit;
  setUp((){
    register = MockRegister();
    authCubit = MockAuthCubit();
    registerCubit = RegisterCubit(register, authCubit);
  });
  tearDown((){
    registerCubit.close();
    authCubit.close();
  });
  blocTest('Should return register success if data from user is correct', build: ()=>registerCubit,
  act: (RegisterCubit registerCubit)async{
    when(register.call(any)).thenAnswer((_) async => Right(true));
    when(authCubit.loginUser(any)).thenAnswer((_) async => true);
    registerCubit.registerUser(RegisterParams(username: 'ardi', email: 'ardiprima27@gmail.com', password: '12345678'));
  },
    expect: ()=>[
      isA<RegisterLoading>(),
      isA<RegisterSuccess>(),
    ]
  );
  blocTest('Should return email is used if user enter duplicate email', build: ()=>registerCubit,
      act: (RegisterCubit registerCubit)async{
        when(register.call(any)).thenAnswer((_) async => Left(AppError(AppErrorType.database,message:'Email has taken')));
        registerCubit.registerUser(RegisterParams(username: 'ardi', email: 'ardiprima27@gmail.com', password: '12345678'));
      },
      expect: ()=>[
        isA<RegisterLoading>(),
        isA<RegisterFailure>(),
      ]
  );
}